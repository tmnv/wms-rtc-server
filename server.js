/**
 * node_module - папка, в которой хранятся модули NPM.
 * package.json - информация о проекте и список зависимостей
 * package-lock.json - список зависимостей проекта. При наличии package.json этот файл не нужен.
 */

const http = require('http');
const https = require('https');
const fs = require('fs');
const WebSocket = require('ws');
const {networkInterfaces} = require('os');

const nets = networkInterfaces();
let myIP = '127.0.0.1';

for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
        // skip over non-ipv4 and internal (i.e. 127.0.0.1) addresses
        if (
            net.family === 'IPv4'
            && ! net.internal
        ) {
            myIP = net.address;
            break;
        }
    }

    if (myIP !== '127.0.0.1') {
        break;
    }
}

// console.log('myIP', myIP);

const ROOT_DIR = __dirname;

const credentials = {
    key: fs.readFileSync(`${ROOT_DIR}/ssl/private.key`, 'utf-8'),
    cert: fs.readFileSync(`${ROOT_DIR}/ssl/certificate.crt`, 'utf-8')
};

const HTTP_PORT = 3080;
const HTTPS_PORT = 3443;

let posUrl = '';

const httpsServer = https.createServer(credentials, (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html; charset=utf-8"
    });
    // console.log('https', req.connection.remoteAddress);
    // console.log('https', req.header('x-forwarded-for')); // это для express
    console.log('HTTPS req.headers.referer', req.headers.referer);
    console.log('HTTPS posUrl', posUrl);

    res.end(`<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Валидация сертификата</title>
</head>
<body>
<h1>Сертификат подтверждён</h1>
<p>Поздравляю, теперь сервер для передачи заказов на склад будет работать.</p>
<!--<p><a href="http://192.168.5.30/pos/order.html">ВЕРНУТЬСЯ В ФОРМУ ОТПРАВКИ ЗАКАЗОВ НА СКЛАД</a></p>-->
<p><a href="${posUrl}pos/order.html">ВЕРНУТЬСЯ В ФОРМУ ОТПРАВКИ ЗАКАЗОВ НА СКЛАД</a></p>
</body>
</html>
    `);
})
httpsServer.listen(HTTPS_PORT);


const httpServer = http.createServer((req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html; charset=utf-8"
    });
    // console.log('http', req.connection.remoteAddress);
    console.log('HTTP req.headers', req.headers);
    console.log('HTTP req.headers.referer', req.headers.referer);
    console.log('HTTP req.url', req.url);
    if (req.url === '/') {
        posUrl = req.headers.referer;
        console.log('HTTP posUrl', posUrl);
    }

    res.end(`<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Валидация сертификата</title>
</head>
<body>
<h1>Подготовка к подтверждению сертификата</h1>
<p>Для того чтобы вы смогли подключиться к серверу передачи заказов на склад, вам нужно подтвердить правильность
сертификата. Для этого перейдите по ссылке ниже и в открывшейся странице подтвердите своё доверие к сертификату.</p>
Нажмите кнопку "ДОПОЛНИТЕЛЬНЫЕ", а потом на ссылку "Перейти на сайт ... (небезопасно)".
<p><a href="https://${myIP}:${HTTPS_PORT}">ПЕРЕЙТИ К ПОДТВЕРЖДЕНИЮ</a></p>
</body>
</html>
    `);
})
httpServer.listen(HTTP_PORT);

const wsServer = new WebSocket.Server({
    server: httpsServer
});

wsServer.on('connection', ws => {
    // console.log('Подключился клиент');
    // console.log(ws);

    ws.on('message', message => {
        // console.log(ws._socket.remoteAddress, message);

        if (message === 'exit') {
            ws.close();
        }
        else {
            wsServer.clients.forEach(client => {
                if (client.readyState === WebSocket.OPEN) {
                    client.send(message);
                }
            });
        }
    });
});

console.log(`Сервер HTTP слушает порт ${HTTP_PORT}.`);
console.log(`Сервер HTTPS слушает порт ${HTTPS_PORT}.`);
console.log(`Сервер WS слушает порт ${HTTPS_PORT}.`);
